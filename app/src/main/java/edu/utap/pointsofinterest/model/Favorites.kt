package edu.utap.pointsofinterest.model

data class Favorite (
    var userID: String = "",
    var favorites: List<String> = listOf()
)