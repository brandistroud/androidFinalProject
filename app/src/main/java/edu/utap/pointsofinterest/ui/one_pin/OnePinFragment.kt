package edu.utap.pointsofinterest.ui.one_pin

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import edu.utap.pointsofinterest.MainViewModel
import edu.utap.pointsofinterest.R
import edu.utap.pointsofinterest.Storage
import edu.utap.pointsofinterest.glide.Glide
import kotlinx.android.synthetic.main.one_pin.*
import java.io.IOException
import java.util.*


class OnePinFragment  : Fragment() {

    private val viewModel: MainViewModel by activityViewModels()
    private var alreadyVotedDown = false
    private var alreadyVotedUp = false
    private var NEW_COMMENT_REQUEST = 1

    companion object {
        fun newInstance(): OnePinFragment {
            return OnePinFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.one_pin, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.findViewById<EditText>(R.id.search)?.visibility= View.INVISIBLE

        val location = viewModel.observeCurrentLocation().value

        val toolbar: ActionBar? = (activity as AppCompatActivity).supportActionBar
        toolbar?.title = location?.name ?: "Location"

        pinName.text = location?.name
        pinCategory.text = location?.category
        pinUser.text = location?.submittedUser

        try {
            val addresses = Geocoder(context, Locale.getDefault()).getFromLocation(
                location?.location?.latitude ?: 0.0, location?.location?.longitude ?: 0.0, 1)
            val address = addresses[0].getAddressLine(0)
            pinLocation.text = address
        } catch (ex: IOException) {
            // Error occurred while creating the File
            Log.d(javaClass.simpleName, "Geocoder giving error", ex)

            // Give a default for demo purposes
            pinLocation.text = "${location?.location?.latitude.toString()}, ${location?.location?.longitude.toString()}"
        }

        val storage = Storage()

        Glide.fetch(storage.uuid2StorageReference(location!!.pictureUUID), pinImage)

        score.text = location.score.toString()
        downVote.setOnClickListener {
            downVote()
        }
        upVote.setOnClickListener {
            upVote()
        }

        val commentsList = location.comment
        pinCommentsNumber.text = commentsList.size.toString()
        val adapter = CommentsAdapter(viewModel)
        commentsRV.layoutManager = LinearLayoutManager(context)
        commentsRV.adapter = adapter
        commentsRV.addItemDecoration(
            DividerItemDecoration(
                commentsRV.context,
                DividerItemDecoration.VERTICAL
            )
        )

        addComments.setOnClickListener {
            val intent = Intent(context, NewComment::class.java)
            startActivityForResult(intent, NEW_COMMENT_REQUEST)
        }

        // Code Reference: RecycleViewFrag
        if (viewModel.isFav(location)) {
            fave.setImageResource(R.drawable.ic_favorite_black_24dp)
        } else {
            fave.setImageResource(R.drawable.ic_favorite_border_black_24dp)
        }

        fave.setOnClickListener {
            if (viewModel.isFav(location)) {
                viewModel.removeFave(location)
                fave.setImageResource(R.drawable.ic_favorite_border_black_24dp)
            } else {
                viewModel.addFave(location)
                fave.setImageResource(R.drawable.ic_favorite_black_24dp)
            }
        }
    }

    private fun downVote () {
        var newScore = score.text.toString().toInt()
        if (!alreadyVotedDown && !alreadyVotedUp) {
            // hasn't voted at all yet
            downVote.setBackgroundColor(Color.RED)
            alreadyVotedDown = true
            newScore = (score.text as String).toInt() - 1
        } else if (alreadyVotedDown) {
            // undo vote down
            downVote.setBackgroundColor(Color.WHITE)
            alreadyVotedDown = false
            newScore = (score.text as String).toInt() + 1
        } else if (alreadyVotedUp) {
            // change vote up to vote down
            downVote.setBackgroundColor(Color.RED)
            upVote.setBackgroundColor(Color.WHITE)
            alreadyVotedDown = true
            alreadyVotedUp = false
            newScore = (score.text as String).toInt() - 2
        }
        score.text = newScore.toString()
        val location = viewModel.observeCurrentLocation().value
        viewModel.updateLocationScore(location!!, newScore.toLong())
    }

    private fun upVote () {
        var newScore = score.text.toString().toInt()
        if (!alreadyVotedDown && !alreadyVotedUp) {
            // hasn't voted at all yet
            upVote.setBackgroundColor(Color.GREEN)
            alreadyVotedUp = true
            newScore = (score.text as String).toInt() + 1
        } else if (alreadyVotedUp) {
            // undo vote up
            upVote.setBackgroundColor(Color.WHITE)
            alreadyVotedUp = false
            newScore = (score.text as String).toInt() - 1
        } else if (alreadyVotedDown) {
            // change vote down to vote up
            downVote.setBackgroundColor(Color.WHITE)
            upVote.setBackgroundColor(Color.GREEN)
            alreadyVotedDown = false
            alreadyVotedUp = true
            newScore = (score.text as String).toInt() + 2
        }
        score.text = newScore.toString()
        val location = viewModel.observeCurrentLocation().value
        viewModel.updateLocationScore(location!!, newScore.toLong())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == NEW_COMMENT_REQUEST && resultCode == Activity.RESULT_OK) {
            val comment = data?.extras?.getString("comment")
            if (!comment.isNullOrBlank()) {
                val location = viewModel.observeCurrentLocation().value
                viewModel.updateLocationComment(location!!, comment)
                commentsRV.adapter?.notifyDataSetChanged()
                val commentsList = location.comment
                pinCommentsNumber.text = commentsList.size.toString()
            }
        }
    }
}