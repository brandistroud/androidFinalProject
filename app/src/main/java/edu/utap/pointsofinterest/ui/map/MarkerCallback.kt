package edu.utap.pointsofinterest.ui.map

// Code Reference: https://stackoverflow.com/questions/24528482/image-not-loading-from-url-in-custom-infowindow-using-picasso-image-loading-libr

import android.util.Log
import com.google.android.gms.maps.model.Marker


class MarkerCallback(marker: Marker?) : com.squareup.picasso.Callback {
    var marker: Marker? = null

    override fun onError() {
        Log.d(javaClass.simpleName, "Error loading thumbnail!")
    }

    override fun onSuccess() {
        if (marker != null && marker!!.isInfoWindowShown) {
            marker!!.hideInfoWindow()
            marker!!.showInfoWindow()
        }
    }

    init {
        this.marker = marker
    }
}