package edu.utap.pointsofinterest.ui.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import edu.utap.pointsofinterest.MainViewModel
import edu.utap.pointsofinterest.R
import edu.utap.pointsofinterest.ui.one_pin.OnePinFragment
import kotlinx.android.synthetic.main.fragment_favorites.*

class FavoritesFragment: Fragment() {

    private val viewModel: MainViewModel by activityViewModels()
    internal lateinit var rowAdapter: FavoritesAdapter

    // Set up the adapter
    // Code Reference: RecyleFrag
    private fun initAdapter(root: View) {
        val rv = root.findViewById<RecyclerView>(R.id.favoritesRV)
        rowAdapter = FavoritesAdapter(viewModel)
        rv.adapter = rowAdapter
        rv.layoutManager = LinearLayoutManager(context)
        val itemDecor = DividerItemDecoration(rv.context, LinearLayoutManager.VERTICAL)
        rv.addItemDecoration(itemDecor)

        rowAdapter.onItemClick = {
            viewModel.setCurrentLocation(it)
            startOnePinFragment()
        }
    }

    // One pin stuff
    private fun startOnePinFragment () {
        val onePin = OnePinFragment.newInstance()

        parentFragmentManager
            .beginTransaction()
            .replace(id, onePin, "onePinTag")
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            .addToBackStack(null)
            .commit()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_favorites, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.findViewById<EditText>(R.id.search)?.visibility= View.INVISIBLE

        initAdapter(view)

        viewModel.observeFavorites().observe(viewLifecycleOwner, Observer {
            rowAdapter.notifyDataSetChanged()
        })
    }
}