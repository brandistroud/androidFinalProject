package edu.utap.pointsofinterest.ui.map

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.facebook.FacebookSdk.getApplicationContext
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter
import com.google.android.gms.maps.model.Marker
import com.squareup.picasso.Picasso
import edu.utap.pointsofinterest.MainViewModel
import edu.utap.pointsofinterest.R
import edu.utap.pointsofinterest.Storage
import edu.utap.pointsofinterest.model.Location


//https://developers.google.com/maps/documentation/android-sdk/infowindows
//http://developine.com/adding-custom-layout-for-google-map-marker-using-custominfo-kotlin-example/

class InfoWindowAdapter(val context: Context, val viewModel: MainViewModel): InfoWindowAdapter {

    private var contentView: View? = null
    private var markersShow: MutableList<Marker> = mutableListOf()

    init {
        contentView = LayoutInflater.from(context).inflate(R.layout.custom_info_window, null, false)
    }

    override fun getInfoContents(marker: Marker): View? {
        val locationObj: Location = marker.tag as Location
        val name = locationObj.name
        val picture = locationObj.pictureUUID
        val category = locationObj.category
        val nameGiven = contentView!!.findViewById<View>(R.id.nameGiven) as TextView
        nameGiven.text = name
        val categoryGiven = contentView!!.findViewById<View>(R.id.categoryGiven) as TextView
        categoryGiven.text = category

        val pictureIV = contentView!!.findViewById<ImageView>(R.id.imageGiven)

        val storage = Storage()

        // Code reference: https://stackoverflow.com/questions/40177250/firebase-how-to-get-image-url-from-firebase-storage
        // Code reference: https://stackoverflow.com/questions/24528482/image-not-loading-from-url-in-custom-infowindow-using-picasso-image-loading-libr
        // This works to show picture, it just takes some time to load, that's all
        storage.uuid2StorageReference(picture).downloadUrl.addOnSuccessListener { uri ->
            val url = uri.toString()

            Picasso.with(getApplicationContext())
                .load(url)
                .into(pictureIV, MarkerCallback(marker))
        }

        return contentView
    }

    override fun getInfoWindow(marker: Marker?): View? {
        return null
    }
}