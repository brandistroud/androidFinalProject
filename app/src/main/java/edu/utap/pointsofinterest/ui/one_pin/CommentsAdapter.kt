package edu.utap.pointsofinterest.ui.one_pin

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import edu.utap.pointsofinterest.MainViewModel
import edu.utap.pointsofinterest.R
import edu.utap.pointsofinterest.ui.categories.CategoriesAdapter
import kotlinx.android.synthetic.main.comment_row.view.*
import java.text.DateFormat

class CommentsAdapter  (private val viewModel: MainViewModel)
    : RecyclerView.Adapter<CommentsAdapter.ViewHolder>() {

    var location = viewModel.observeCurrentLocation().value
    private var expandedList = mutableMapOf<Int, Boolean>()
    //private val dateFormat: DateFormat = android.icu.text.SimpleDateFormat("hh:mm:ss MM-dd-yyyy")

    inner class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {
        val comment = row.text
        val byline = row.byline

        init { }

        fun bind(position: Int) {
            var text = location?.comment?.get(position) ?: ""
            val index = text.lastIndexOf(" — ")
            comment.text = text.subSequence(0, index)
            byline.text = text.subSequence(index, text.length)
        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CommentsAdapter.ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.comment_row, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return location?.comment?.size ?: 0
    }

    override fun onBindViewHolder(holder: CommentsAdapter.ViewHolder, position: Int) {
        holder.bind(position)
    }

}