package edu.utap.pointsofinterest.ui.one_pin

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import edu.utap.pointsofinterest.R
import kotlinx.android.synthetic.main.fragment_new_comment.*

class NewComment : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_new_comment)

        findViewById<EditText>(R.id.search)?.visibility= View.INVISIBLE

        submitNewComment.setOnClickListener {
            val comment = inputNewComment.text.toString()
            if (!comment.isNullOrBlank()) {
                val result = Intent()
                result.putExtra("comment", comment)
                setResult(Activity.RESULT_OK, result)
                finish()
            }
        }
    }
}