package edu.utap.pointsofinterest.ui.logout

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.activityViewModels
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import edu.utap.pointsofinterest.Auth
import edu.utap.pointsofinterest.MainActivity
import edu.utap.pointsofinterest.MainViewModel
import edu.utap.pointsofinterest.R
import edu.utap.pointsofinterest.ui.map.MapFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_logout.*
import kotlinx.android.synthetic.main.fragment_map.*

class LogoutFragment : Fragment() {

    private val viewModel: MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_logout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.findViewById<EditText>(R.id.search)?.visibility= View.INVISIBLE

        logout_no.setOnClickListener {
            // TODO this technically always goes back to map rather than previous, but not buggy
            parentFragmentManager
                .beginTransaction()
                .replace(id, MapFragment())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit()

            val navView: NavigationView = (activity as MainActivity).findViewById(R.id.nav_view)
            navView.menu.getItem(0).isChecked = true
        }

        logout_yes.setOnClickListener {

            viewModel.setCategory("All of the Above")

            parentFragmentManager
                .beginTransaction()
                .replace(id, MapFragment())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit()

            val navView: NavigationView = (activity as MainActivity).findViewById(R.id.nav_view)
            navView.menu.getItem(0).isChecked = true

            Auth((activity as MainActivity)).signOut()
        }
    }
}