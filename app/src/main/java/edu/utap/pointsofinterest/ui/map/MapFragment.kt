package edu.utap.pointsofinterest.ui.map

import android.Manifest
import android.content.Context.LOCATION_SERVICE
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import edu.utap.pointsofinterest.*
import edu.utap.pointsofinterest.model.Location
import edu.utap.pointsofinterest.ui.one_pin.OnePinFragment


class MapFragment : Fragment(), OnMapReadyCallback {

    private val viewModel: MainViewModel by activityViewModels()
    private val locationRequestCode = 101
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var mMap: GoogleMap
    // UT as default location
    private var currLocation = LatLng(30.2849, -97.7341)
    var markers = mutableListOf<Marker>()

    private fun addMarkerToMap(locationGiven: Location) {
        if (this::mMap.isInitialized) {
            val locationPoint =
                LatLng(locationGiven.location.latitude, locationGiven.location.longitude)
            val category = locationGiven.category
            val catIndex = categories.indexOf(category)
            val catColor = category_colors[catIndex]

            val newMarker: Marker = mMap.addMarker(
                MarkerOptions()
                .position(locationPoint)
                .icon(BitmapDescriptorFactory.defaultMarker(catColor)))
            newMarker.tag = locationGiven
            markers.add(newMarker)
        }
    }

    private fun clearMarkers() {
        for (i in 0 until markers.size) {
            markers[i].remove()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Code reference: Maps Demo
        if (ContextCompat.checkSelfPermission(context!!,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            // XXX enable location and enable UI for my location
            mMap.isMyLocationEnabled = true
        } else {
            requestPermission(
                Manifest.permission.ACCESS_FINE_LOCATION,
                locationRequestCode)
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context!!)

        fusedLocationClient.lastLocation.addOnSuccessListener {
            if (it != null) {
                currLocation = LatLng(it.latitude, it.longitude)
            }
        }

        // Zoom to UT Area for now
        val ut = LatLng(30.2849, -97.7341)
        val wc = LatLng(29.137714999999996, -95.6509114) // TODO testing with other town

        val zoomLevel = 15.0f
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currLocation, zoomLevel))

        mMap.setInfoWindowAdapter(InfoWindowAdapter(context!!, viewModel))
        mMap.setOnInfoWindowClickListener {
            val location = it.tag as Location
            viewModel.setCurrentLocation(location)
            startOnePinFragment()
        }

        viewModel.observeLocations().observe(viewLifecycleOwner, Observer {
            clearMarkers()
            for (i in it.indices) {
                addMarkerToMap(it[i])
            }
        })
    }

    // Code Reference: https://stackoverflow.com/questions/30991087/mapfragment-getmapasyncthis-nullpointerexception
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_map, null, false)
        val mapFragment = this.childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?

        val toolbar: ActionBar? = (activity as AppCompatActivity).supportActionBar
        toolbar?.title = "Map"

        // Set up the map, zoom to UT for now
        mapFragment?.onCreate(savedInstanceState)
        mapFragment?.getMapAsync(this)

        activity?.findViewById<EditText>(R.id.search)?.visibility=VISIBLE
        search()

        return view
    }

    //--------------------------------------------------------------------------------------------//
    // One pin stuff
    private fun startOnePinFragment () {
        val onePin = OnePinFragment.newInstance()

        parentFragmentManager
            .beginTransaction()
            .replace(id, onePin, "onePinTag")
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            .addToBackStack(null)
            .commit()
    }

    // Code Reference Maps Demo
    private fun requestPermission(permissionType: String,
                                  requestCode: Int) {
        ActivityCompat.requestPermissions(activity as MainActivity,
            arrayOf(permissionType), requestCode
        )
    }

    // Code Reference Maps Demo
    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            locationRequestCode -> {
                if (grantResults.isEmpty() || grantResults[0] !=
                    PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(context,
                        "Unable to show location - permission required",
                        Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    // Code reference: https://www.androdocs.com/java/getting-current-location-latitude-longitude-in-android-using-java.html
    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager =
            context!!.getSystemService(LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    //--------------------------------------------------------------------------------------------//
    // Search stuff
    private fun search () {
        activity?.findViewById<EditText>(R.id.search)?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(
                s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (context != null) {
                    viewModel.setSearchTerm(s.toString(), context!!)
                } else {
                    println("Search; Context is null")
                }
            }
        })
    }
}
