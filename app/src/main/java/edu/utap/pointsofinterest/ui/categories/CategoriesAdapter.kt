package edu.utap.pointsofinterest.ui.categories

import android.media.Image
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.MutableLiveData
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.FirebaseFirestore
import edu.utap.pointsofinterest.MainViewModel
import edu.utap.pointsofinterest.R
import edu.utap.pointsofinterest.categories
import edu.utap.pointsofinterest.category_images
import edu.utap.pointsofinterest.model.Location
import kotlinx.android.synthetic.main.category_row.view.*
import org.w3c.dom.Text

class CategoriesAdapter (private val viewModel: MainViewModel)
    : RecyclerView.Adapter<CategoriesAdapter.ViewHolder>() {

    inner class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {
        var image = row.findViewById<ImageView>(R.id.category_image)
        var title = row.findViewById<TextView>(R.id.category_title)
        var count = row.findViewById<TextView>(R.id.category_count)

        init {
            row.setOnClickListener {
                viewModel.setCategory(title.text.toString())
                val navController = findNavController(row)
                navController.popBackStack()
            }
        }

        fun bind(position: Int) {
            image.setImageResource(category_images[position])
            title.text = categories[position]
            count.text = catCount(categories[position])
        }

        private fun catCount (cat : String) : String {
            var counter = 0
            viewModel.observeAllLocations()
                .value?.forEach {
                // Special case for All
                if (cat == categories[8]) {
                    counter++
                }
                if (it.category == cat) {
                    counter++
                }
            }
            return counter.toString()
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CategoriesAdapter.ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.category_row, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CategoriesAdapter.ViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return categories.size
    }
}
