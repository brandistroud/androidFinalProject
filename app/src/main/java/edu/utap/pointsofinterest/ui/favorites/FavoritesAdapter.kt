package edu.utap.pointsofinterest.ui.favorites

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import edu.utap.pointsofinterest.MainViewModel
import edu.utap.pointsofinterest.R
import edu.utap.pointsofinterest.Storage
import edu.utap.pointsofinterest.glide.Glide
import edu.utap.pointsofinterest.model.Location
import edu.utap.pointsofinterest.ui.one_pin.OnePinFragment

class FavoritesAdapter(private val viewModel: MainViewModel):
    RecyclerView.Adapter<FavoritesAdapter.ViewHolder>() {

    var onItemClick: ((Location) -> Unit)? = null

    inner class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {
        internal val locationImage = itemView.findViewById<ImageView>(R.id.imageGiven)
        internal val locationName = itemView.findViewById<TextView>(R.id.nameGiven)
        internal val locationCat = itemView.findViewById<TextView>(R.id.categoryGiven)
        internal val faveIcon = itemView.findViewById<ImageButton>(R.id.fave)

        // Code reference for row click: https://stackoverflow.com/questions/29424944/recyclerview-itemclicklistener-in-kotlin
        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(viewModel.getFavorite(adapterPosition))
            }
        }

        fun bind(location: Location) {
            val name = location.name
            val picture = location.pictureUUID
            val category = location.category
            locationName.text = name
            locationCat.text = category

            val storage = Storage()

            // This works to show picture, it just takes some time to load, that's all
            Glide.fetch(storage.uuid2StorageReference(picture), locationImage)

            faveIcon.setOnClickListener {
                viewModel.removeFave(location)
            }
        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FavoritesAdapter.ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.favorites_row, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: FavoritesAdapter.ViewHolder, position: Int) {
        holder.bind(viewModel.getFavorite(position))
    }

    override fun getItemCount(): Int {
        return viewModel.getFavoriteSize()
    }
}