package edu.utap.pointsofinterest.ui.new_entry

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.location.Geocoder
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import edu.utap.pointsofinterest.*
import edu.utap.pointsofinterest.ui.one_pin.NewComment
import edu.utap.pointsofinterest.ui.one_pin.OnePinFragment
import kotlinx.android.synthetic.main.fragment_new_entry.*
import kotlinx.android.synthetic.main.one_pin.*
import java.io.IOException
import java.util.*

class NewEntryFragment : Fragment(), AdapterView.OnItemSelectedListener {

    private val viewModel: MainViewModel by activityViewModels()
    private  val db = Firebase.firestore
    private var toast : Toast? = null
    private lateinit var category: String
    private lateinit var imagePath : String
    private lateinit var imageUUID: String
    private lateinit var userName : String

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    companion object {
        private const val IMAGE_PICK_CODE = 1000;
        private const val PERMISSION_CODE = 1001;
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_new_entry, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.findViewById<EditText>(R.id.search)?.visibility= View.INVISIBLE

        chooseCategory?.onItemSelectedListener = this
        val sub = IntRange(0, categories.size - 2)
        val selectableCats = categories.sliceArray(sub)
        val adapter = ArrayAdapter(this.context!!, android.R.layout.simple_spinner_item, selectableCats)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        chooseCategory?.adapter = adapter

        submit.setOnClickListener { submit() }

        uploadImage.setOnClickListener { //check runtime permission
            if (VERSION.SDK_INT >= VERSION_CODES.M){
                if (checkSelfPermission(this.context!!, Manifest.permission.READ_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_DENIED){
                    //permission denied
                    val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                    //show popup to request runtime permission
                    requestPermissions(permissions, PERMISSION_CODE);
                }
                else{
                    //permission already granted
                    startUpload();
                }
            }
            else{
                //system OS is < Marshmallow
                startUpload();
            }
        }

        takeImage.setOnClickListener { viewModel.takePhoto(::pictureSuccess) }

        currLocation.setOnClickListener {
            useCurrLocation()
        }

        userName = Auth(activity as MainActivity).getDisplayName()
        usernameTV.text = userName
    }

    private fun pictureSuccess(path: String, uuid: String) {
        // Add new image path to imageList
        Log.v("SADBOI", "in picture success")
        imagePath = path
        imageUUID = uuid
        val bitmap = BitmapFactory.decodeFile(path)
        inputImage.setImageBitmap(bitmap)
    }

    //--------------------------------------------------------------------------------------------//
    // Submit

    private fun toaster(msg: String) {
        if (toast != null) {
            toast!!.cancel()
        }
        toast = Toast.makeText(context, msg, Toast.LENGTH_LONG)
        toast!!.show()
    }

    fun submit() {
        val name = inputName.text
        val comment = inputComments.text
        val location = locationInput.text
        if (this::imagePath.isInitialized) {
            if (name.isNullOrBlank()
                || comment.isNullOrBlank()
                || imagePath.isNullOrBlank()
                || imageUUID.isNullOrBlank()
                || location.isNullOrBlank()
            ) {
                // ERROR, not all fields are filled
                toaster("ERROR: You must fill in all fields.")
            } else {
                // good to go, add entry to db
                viewModel.createLocation(
                    name.toString(),
                    location.toString(),
                    imageUUID,
                    category,
                    comment.toString()
                )
                val navController = Navigation.findNavController(this.view!!)
                navController.popBackStack()
            }
        } else {
            // ERROR, not all fields are filled
            toaster("ERROR: You must fill in all fields.")
        }
    }

    //--------------------------------------------------------------------------------------------//
    // Spinner stuff

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        category = categories[p2]
    }

    //--------------------------------------------------------------------------------------------//
    // Picture gallery/upload stuff

    private fun startUpload() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    //handle requested permission result
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.size >0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup granted
                    startUpload ()
                }
                else{
                    //permission from popup denied
                    toaster("Permission denied")
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == IMAGE_PICK_CODE && resultCode == RESULT_OK) {
            val selectedImage = data!!.data
            if (selectedImage != null) {
                inputImage.setImageURI(selectedImage)
                val galleryUUID = UUID.randomUUID().toString()
                imagePath = selectedImage.toString()
                imageUUID = galleryUUID
                viewModel.gallerySuccess(selectedImage, galleryUUID)
            }
        }

    }

    //--------------------------------------------------------------------------------------------//
    // Grab current User's Location

    private fun useCurrLocation() {
        // We should already have location permissions if app was able to open (since map opens first to get these)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context!!)

        fusedLocationClient.lastLocation.addOnSuccessListener {
            if (it != null) {
                try {
                    val addresses = Geocoder(context, Locale.getDefault()).getFromLocation(it.latitude ?: 0.0, it.longitude ?: 0.0, 1);
                    locationInput.setText(addresses[0].getAddressLine(0))
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    Log.d(javaClass.simpleName, "Geocoder giving error", ex)
                    // Place default information for demo purposes
                    locationInput.setText("2308 Whitis Ave, Austin, TX 78712")
                }
            }
        }
    }


}
