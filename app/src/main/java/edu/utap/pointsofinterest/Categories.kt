package edu.utap.pointsofinterest

import com.google.android.gms.maps.model.BitmapDescriptorFactory

val categories = arrayOf("Misc", "Food", "Recreation", "Nap", "Study",
                        "Social", "Shopping", "Other", "All of the Above")
val category_images = arrayOf(R.drawable.ic_misc_foreground, R.drawable.ic_food_foreground,
                                R.drawable.ic_rec_foreground, R.drawable.ic_nap_foreground,
                                R.drawable.ic_study_foreground, R.drawable.ic_social_foreground,
                                R.drawable.ic_shopping_foreground, R.drawable.ic_brush_foreground,
                                R.drawable.ic_all_foreground)
val category_colors = arrayOf(BitmapDescriptorFactory.HUE_AZURE, BitmapDescriptorFactory.HUE_BLUE,
                                BitmapDescriptorFactory.HUE_CYAN, BitmapDescriptorFactory.HUE_GREEN,
                                BitmapDescriptorFactory.HUE_MAGENTA, BitmapDescriptorFactory.HUE_ORANGE,
                                BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_VIOLET,
                                BitmapDescriptorFactory.HUE_YELLOW)