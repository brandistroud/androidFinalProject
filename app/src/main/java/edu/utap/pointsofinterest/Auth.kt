package edu.utap.pointsofinterest

import android.util.Log
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest

class Auth(activity: MainActivity) {

    companion object {
        const val RC_SIGN_IN = 123
    }

    var givenActivity: MainActivity = activity
    var user = FirebaseAuth.getInstance().currentUser

    // Choose authentication providers
    private val providers = arrayListOf(
        AuthUI.IdpConfig.EmailBuilder().build(),
        AuthUI.IdpConfig.PhoneBuilder().build(),
        AuthUI.IdpConfig.GoogleBuilder().build(),
        AuthUI.IdpConfig.TwitterBuilder().build(),
        AuthUI.IdpConfig.FacebookBuilder().build())

    init {
        // Check to see if a user is already logged in on this device, if not open Login Activity
        FirebaseAuth.AuthStateListener {
            user = FirebaseAuth.getInstance().currentUser
        }
        if (user != null) {
            // User is signed in
        } else {
            // No user is signed in
            // Code reference: https://github.com/firebase/snippets-android/blob/2bd81c257d97823f57440bd2b12e345d2c4f7be4/auth/app/src/main/java/com/google/firebase/quickstart/auth/kotlin/FirebaseUIActivity.kt#L21-L35
            startAuthPage()
        }
    }

    private fun startAuthPage() {
        // Create and launch sign-in intent
        givenActivity.startActivityForResult(
            AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setLogo(R.drawable.ic_logo)
                .setAvailableProviders(providers)
                .build(),
            RC_SIGN_IN
        )
        // [END auth_fui_create_intent]
    }

    fun getDisplayName(): String {
        user = FirebaseAuth.getInstance().currentUser
        if (user == null) {
            return "Current User"
        } else {
            return user!!.displayName.toString()
        }
    }

    fun setDisplayName() {
        if (user != null && user!!.displayName == null) {
            val profileUpdates = UserProfileChangeRequest.Builder()
                .setDisplayName("Random User")
                .build()

            user!!.updateProfile(profileUpdates)
        }
    }

    fun signOut() {
        // Sign the user out, have them re-sign in to be able to interact with app
        FirebaseAuth.getInstance().signOut()
        startAuthPage()
    }

}