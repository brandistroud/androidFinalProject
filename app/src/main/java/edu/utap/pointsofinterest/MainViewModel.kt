package edu.utap.pointsofinterest

import android.app.Application
import android.content.ContentValues
import android.content.Context
import android.location.Geocoder
import android.media.Image
import android.net.Uri
import android.os.Environment
import android.util.Log
import androidx.core.content.FileProvider
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.GeoPoint
import edu.utap.pointsofinterest.model.Favorite
import edu.utap.pointsofinterest.model.Location
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class MainViewModel (application: Application) : AndroidViewModel(application) {
    private val appContext = getApplication<Application>().applicationContext
    private var photoFile: File? = null
    private var storageDir = getApplication<Application>().getExternalFilesDir(Environment.DIRECTORY_PICTURES)

    private var takePhotoIntent: ()->Unit = ::noPhoto
    private var photoSuccess: (path: String, uuid: String)->Unit = ::defaultPhoto
    private var pictureUUID: String =""

    private var locationList = MutableLiveData<List<Location>>()
    private var fullLocationList = MutableLiveData<List<Location>>()
    private var favoritesList = MutableLiveData<List<Location>>()
    private val dbHelp = ViewModelDBHelper(locationList, fullLocationList, favoritesList, FirebaseAuth.getInstance().currentUser?.uid ?: "")

    private var currentCategory = categories[8]

    private var currentLocation = MutableLiveData<Location>()

    private var searchTerm = MutableLiveData<String>()

    //--------------------------------------------------------------------------------------------//
    // Camera stuff

    private fun noPhoto() {
        Log.d(javaClass.simpleName,"Function must be initialized to something that can start the camera intent")
    }

    private fun defaultPhoto(@Suppress("UNUSED_PARAMETER") path: String, @Suppress("UNUSED_PARAMETER") uuid: String) {
        Log.d(javaClass.simpleName,"Function must be initialized to photo callback")
    }

    fun setPhotoIntent(_takePhotoIntent: ()->Unit) {
        takePhotoIntent = _takePhotoIntent
    }

    fun takePhoto(_photoSuccess: (String, String)->Unit) {
        photoSuccess = _photoSuccess

        takePhotoIntent()
    }

    fun getPhotoURI(): Uri {
        // Create an image file name
        pictureUUID = UUID.randomUUID().toString()
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
        var photoUri : Uri? = null
        // Create the File where the photo should go
        try {
            val localPhotoFile = File(storageDir, "${pictureUUID}.jpg")
            photoFile = localPhotoFile
            Log.d(javaClass.simpleName, "photo path ${localPhotoFile.absolutePath}")
            photoUri = FileProvider.getUriForFile(
                appContext,
                "edu.utap.pointsofinterest",
                localPhotoFile)
        } catch (ex: IOException) {
            // Error occurred while creating the File
            Log.d(javaClass.simpleName, "Cannot create file", ex)
        }
        // CRASH.  Production code should do something more graceful
        return photoUri!!
    }

    fun gallerySuccess(givenURI: Uri, givenUUID: String) {
        pictureUUID = givenUUID

        val storage = Storage()

        storage.uploadGalleryPhoto(givenURI, pictureUUID) {
            photoFile = null
            photoSuccess = ::defaultPhoto
        }
    }

    fun pictureSuccess() {
        assert(photoFile != null)

        val storage = Storage()

        // Wait until photo is successfully uploaded before calling back
        storage.uploadImage(photoFile!!, pictureUUID) {
            photoSuccess(photoFile!!.absolutePath, pictureUUID)
            photoFile = null
            photoSuccess = ::defaultPhoto
        }
    }

    fun pictureFailure() {
        // Note, the camera intent will only create the file if the user hits accept
        photoFile = null
        pictureUUID = ""
    }

    //--------------------------------------------------------------------------------------------//
    // Location stuff

    // Code Reference: FireNote Demo Code
    /////////////////////////////////////////////////////////////
    // Locations, memory cache and database interaction
    fun observeLocations(): LiveData<List<Location>> {
        return locationList
    }

    fun observeAllLocations(): LiveData<List<Location>> {
        return fullLocationList
    }

    fun isLocationsEmpty(): Boolean {
        return locationList.value.isNullOrEmpty()
    }

    // Get a location from the memory cache
    fun getLocation(position: Int) : Location {
        val location = locationList.value?.get(position)
        return location!!
    }

    // After we successfully modify the db, we refetch the contents to update our
    // live data.  Hence we always pass in locationsList
    fun updateLocationScore(location: Location, score: Long) {
        location.score = score
        dbHelp.updateLocation(location, locationList, currentCategory)
    }

    fun updateLocationComment(location: Location, newComment: String) {
        var currComments = location.comment.toMutableList()
        val username = try {
            FirebaseAuth.getInstance().currentUser!!.displayName.toString()
        } catch (ex: IOException) {
            "Unknown"
        }
        val newCom = newComment + " — " + username
        currComments.add(newCom)
        location.comment = currComments
        dbHelp.updateLocation(location, locationList, currentCategory)
    }

    fun createLocation(nameGiven: String, locationGiven: String, pictureUUIDGiven: String, categoryGiven: String, commentGiven: String) {
        // First, change address given into a geocode
        // https://developers.google.com/maps/documentation/geocoding/start?csw=1#Geocoding
        // NOTE: Sometimes the following line results in an error?? Sometimes it doesn't??
        val username = try {
            FirebaseAuth.getInstance().currentUser!!.displayName.toString()
        } catch (ex: IOException) {
            "Unknown"
        }
        val comGiven = commentGiven + " — " + username
        val comments = listOf(comGiven)
        try {
            val address = Geocoder(appContext).getFromLocationName(locationGiven,5)

            if (address != null) {
                val locationFound = address[0]

                val geoPoint = GeoPoint(locationFound.latitude, locationFound.longitude)
                // Location ID will be set in DB Help Method
                val location = Location(
                    name = nameGiven,
                    location = geoPoint,
                    comment = comments,
                    category = categoryGiven,
                    pictureUUID = pictureUUIDGiven,
                    submittedUser = username
                    // Score in model is set to default zero, so don't need to pass in
                )
                dbHelp.createLocation(location, locationList, fullLocationList)
            } else {
                // User gave an invalid address
            }
        } catch (ex: IOException) {
            // Error occurred while creating the File
            Log.d(javaClass.simpleName, "Geocoder giving error", ex)

            // Give defaults on error for demo purposes

            val geoPoint = GeoPoint(30.289780, -97.740310)
            // Location ID will be set in DB Help Method
            val location = Location(
                name = nameGiven,
                location = geoPoint,
                comment = comments,
                category = categoryGiven,
                pictureUUID = pictureUUIDGiven,
                submittedUser = FirebaseAuth.getInstance().currentUser!!.displayName.toString()
                // Score in model is set to default zero, so don't need to pass in
            )
            dbHelp.createLocation(location, locationList, fullLocationList)
        }
    }

    // I don't think we will ever want to remove a location from in app

    //--------------------------------------------------------------------------------------------//
    // One pin stuff
    fun setCurrentLocation (location: Location) {
        currentLocation.postValue(location)
    }

    fun observeCurrentLocation () : LiveData<Location> {
        return currentLocation
    }

    //--------------------------------------------------------------------------------------------//
    // Category stuff
    // When working on it, didn't see a need for live data (once you set, just auto update DB call here?
    fun setCategory(cat : String) {
        dbHelp.fetchACategory(locationList, cat)
        currentCategory = cat
    }

    //--------------------------------------------------------------------------------------------//
    // Favorites Stuff

    fun observeFavorites(): LiveData<List<Location>> {
        return favoritesList
    }

    // Get a location from the memory cache
    fun getFavoriteSize() : Int {
        val result = favoritesList.value?.size
        if (result != null) {
            return result
        } else {
            return 0
        }
    }

    // Get a location from the memory cache
    fun getFavorite(position: Int) : Location {
        val location = favoritesList.value?.get(position)
        return location!!
    }

    fun addFave(loc: Location) {
        val localFaves = locToIDFaves()
        localFaves.add(loc.locationID)
        val newFave = Favorite(
            userID = FirebaseAuth.getInstance().currentUser!!.uid,
            favorites = localFaves.toList())
        dbHelp.updateFavorites(newFave, favoritesList)
    }

    fun removeFave(loc: Location) {
        val localFaves = locToIDFaves()
        localFaves.remove(loc.locationID)
        val newFave = Favorite(
            userID = FirebaseAuth.getInstance().currentUser!!.uid,
            favorites = localFaves.toList())
        dbHelp.updateFavorites(newFave, favoritesList)
    }

    private fun locToIDFaves(): MutableList<String> {
        val localFaves = mutableListOf<String>()
        for (elem in 0 until getFavoriteSize()) {
            val currFav = getFavorite(elem)
            localFaves.add(currFav.locationID)
        }
        return localFaves
    }

    fun isFav(location: Location): Boolean {
        return favoritesList.value?.contains(location) ?: false
    }

    //--------------------------------------------------------------------------------------------//
    // Search Stuff
    fun setSearchTerm(s: String, c: Context) {
        searchTerm.value = s
        if (s.isNullOrBlank()) {
            dbHelp.fetchACategory(locationList, currentCategory)
        } else {
            locationList.value = filterList(s, locationList.value!!, c)
        }
    }

    fun observeSearchTerm() : LiveData<String> {
        return searchTerm
    }

    private fun filterList(searchTerm: String?, value: List<Location>, context: Context): List<Location> {
        if(searchTerm == null || searchTerm.isEmpty()) return value

        return value.filter {
            try {
                val addresses = Geocoder(context, Locale.getDefault()).getFromLocation(
                    it.location.latitude ?: 0.0, it.location.longitude ?: 0.0, 1
                )
                val address = addresses[0].getAddressLine(0)
                var foundIt = it.submittedUser.contains(searchTerm, ignoreCase = true) ||
                        it.category.contains(searchTerm, ignoreCase = true) ||
                        it.name.contains(searchTerm, ignoreCase = true) ||
                        address.contains(searchTerm, ignoreCase = true)
                return@filter foundIt
            } catch (ex: IOException) {
                Log.d(javaClass.simpleName, "Geocoder giving error", ex)

                // Submit something default for demo purposes
                val address = "2317 Speedway, Austin, TX 78712"
                var foundIt = it.submittedUser.contains(searchTerm, ignoreCase = true) ||
                        it.category.contains(searchTerm, ignoreCase = true) ||
                        it.name.contains(searchTerm, ignoreCase = true) ||
                        address.contains(searchTerm, ignoreCase = true)
                return@filter foundIt
            }
        }
    }
}