package edu.utap.pointsofinterest

// ALL CODE REFERENCED FROM FireNote DEMO CODE

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import edu.utap.pointsofinterest.model.Favorite
import edu.utap.pointsofinterest.model.Location

class ViewModelDBHelper(
    locationsList: MutableLiveData<List<Location>>,
    fullLocationList: MutableLiveData<List<Location>>,
    favoritesList: MutableLiveData<List<Location>>,
    userID: String
) {
    private val db: FirebaseFirestore = FirebaseFirestore.getInstance()

    init {
        dbFetchLocations(locationsList, categories[8])
        dbFetchAllLocations(fullLocationList)
        dbFetchFavorites(favoritesList, userID)
    }

    private fun elipsizeString(string: String) : String {
        if(string.length < 10)
            return string
        return string.substring(0..9) + "..."
    }

    /////////////////////////////////////////////////////////////
    // Interact with Firestore db
    // https://firebase.google.com/docs/firestore/query-data/get-data
    //
    // If we want to listen for real time updates use this
    // .addSnapshotListener { querySnapshot, firebaseFirestoreException ->
    // But be careful about how listener updates live data
    // and locationListener?.remove() in onCleared
    private fun dbFetchLocations(locationsList: MutableLiveData<List<Location>>, categoryGiven: String) {
        // We don't need a limit or ordering

        // Grab All
        if (categoryGiven == categories[8]) {
            dbFetchAllLocations(locationsList)
        } else {
            db.collection("locations")
                .whereEqualTo("category", categoryGiven)
                .get()
                .addOnSuccessListener { result ->
                    Log.d(javaClass.simpleName, "allLocations fetch category ${result!!.documents.size}")
                    locationsList.value = result.documents.mapNotNull {
                        it.toObject(Location::class.java)
                    }
                }
                .addOnFailureListener {
                    Log.d(javaClass.simpleName, "allNotes category fetch FAILED ", it)
                }
        }
    }

    private fun dbFetchAllLocations(locationsList: MutableLiveData<List<Location>>) {
        db.collection("locations")
            .get()
            .addOnSuccessListener { result ->
                Log.d(javaClass.simpleName, "allLocations fetch ${result!!.documents.size}")
                locationsList.value = result.documents.mapNotNull {
                    it.toObject(Location::class.java)
                }
            }
            .addOnFailureListener {
                Log.d(javaClass.simpleName, "allNotes fetch FAILED ", it)
            }
    }

    fun fetchACategory(locationsList: MutableLiveData<List<Location>>, categoryGiven: String) {
        dbFetchLocations(locationsList, categoryGiven)
    }

    fun createLocation(
        location: Location,
        locationsList: MutableLiveData<List<Location>>,
        fullLocationList: MutableLiveData<List<Location>>
    ) {

        location.locationID = db.collection("locations").document().id

        db.collection("locations")
            .document(location.locationID)
            .set(location)
            .addOnSuccessListener {
                Log.d(
                    javaClass.simpleName,
                    "Location create \"${elipsizeString(location.name)}\" id: ${location.locationID}"
                )
                dbFetchLocations(locationsList, categories[8])
                dbFetchAllLocations(fullLocationList)
            }
            .addOnFailureListener { e ->
                Log.d(javaClass.simpleName, "Location create FAILED \"${elipsizeString(location.name)}\"")
                Log.w(javaClass.simpleName, "Error ", e)
            }
    }

    // After we successfully modify the db, we refetch the contents to update our
    // live data.  Hence the dbFetchLocations() calls below.
    fun updateLocation(
        location: Location,
        locationList: MutableLiveData<List<Location>>,
        currCat: String
    ) {
        // XXX Writing a Location
        db.collection("locations")
            .document(location.locationID)
            .set(location)
            .addOnSuccessListener {
                Log.d(
                    javaClass.simpleName,
                    "Location update \"${elipsizeString(location.name)}\" id: ${location.locationID} " +
                            "score:${location.score} comments size: ${location.comment.size}"
                )
                dbFetchLocations(locationList, currCat)
            }
            .addOnFailureListener { e ->
                Log.d(javaClass.simpleName, "Location update FAILED \"${elipsizeString(location.name)}\"")
                Log.w(javaClass.simpleName, "Error ", e)
            }
    }

    /////////////////////////////////////////////////////////////
    // Favorites logic

    private fun dbFetchFavorites(favoritesList: MutableLiveData<List<Location>>, uid: String) {
        if (uid != "") {
            // We don't need a limit or ordering
            db.collection("favorites")
                .document(uid)
                .get()
                .addOnSuccessListener { result ->
                    if (result != null) {
                        Log.d(javaClass.simpleName, "DocumentSnapshot data: ${result.data}")
                        val docFaves = result.toObject(Favorite::class.java)?.favorites
                        if (docFaves != null && docFaves.isNotEmpty()) {
                            dbFetchLocationFavorites(favoritesList, docFaves)
                        } else {
                            // There should never be a location with id "" so this is like grabbing an empty list
                            dbFetchLocationFavorites(favoritesList, listOf(""))
                        }
                    } else {
                        Log.d(javaClass.simpleName, "No such document")
                    }
                }
                .addOnFailureListener {
                    Log.d(javaClass.simpleName, "favorites fetch FAILED ", it)
                }
        }
    }

    private fun dbFetchLocationFavorites(favoritesList: MutableLiveData<List<Location>>, locationIDs: List<String>) {
        db.collection("locations")
            .whereIn("locationID", locationIDs)
            .get()
            .addOnSuccessListener { result ->
                Log.d(javaClass.simpleName, "locationFavorites fetch ${result!!.documents.size}")
                favoritesList.value = result.documents.mapNotNull {
                    it.toObject(Location::class.java)
                }
            }
            .addOnFailureListener {
                Log.d(javaClass.simpleName, "locationsFavorites fetch FAILED ", it)
            }
    }

    fun updateFavorites(
        faves: Favorite,
        favoritesList: MutableLiveData<List<Location>>) {

        // XXX Writing a Location
        db.collection("favorites")
            .document(faves.userID)
            .set(faves)
            .addOnSuccessListener {
                Log.d(
                    javaClass.simpleName,
                    "Faves update"
                )
                dbFetchFavorites(favoritesList, faves.userID)
            }
            .addOnFailureListener { e ->
                Log.d(javaClass.simpleName, "Faves update FAILED")
                Log.w(javaClass.simpleName, "Error ", e)
            }
    }
}
